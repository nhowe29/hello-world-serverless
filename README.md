# hello-world-serverless

This project contains the core source code and supporting files for the demo hello world AWS API.

- src/main - Code for the application's Lambda function.
- yaml template - The template that defines the application's AWS resources.

The application uses various AWS resources, including Lambda functions and an API Gateway API. These resources are defined in the `yaml` file in this project.

If you prefer to use an integrated development environment (IDE) to build and test your application, you can use the AWS Toolkit.  

* [IntelliJ](https://docs.aws.amazon.com/toolkit-for-jetbrains/latest/userguide/welcome.html)

## Deployment

The Serverless Application Model Command Line Interface (SAM CLI) is an extension of the AWS CLI that adds functionality for building and testing Lambda applications. It uses Docker to run your functions in an Amazon Linux environment that matches Lambda. It can also emulate your application's build environment and API for local testing.

To use the SAM CLI, you need the following tools.

* AWS CLI - [Install the AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) and [configure it with your AWS credentials].
* SAM CLI - [Install the SAM CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)
* Node.js - [Install Node.js 10](https://nodejs.org/en/), including the NPM package management tool.
* Docker - [Install Docker community edition](https://hub.docker.com/search/?type=edition&offering=community)

The SAM CLI uses an Amazon S3 bucket to store your application's deployment artifacts.


To prepare the application for deployment, use the `sam package` command.

```bash
app$ sam package \
    --template-file aws-cloudformation/hello-world-serverless-template.yml \
    --output-template-file packaged.yml \
    --s3-bucket treehouse-lambda-deploy
```

The SAM CLI creates deployment packages, uploads them to the S3 bucket, and creates a new version of the template that refers to the artifacts in the bucket. 

To deploy the application, use the `sam deploy` command.

```bash
app$ sam deploy \
    --template-file packaged.yml \
    --stack-name hello-world-serverless \
    --capabilities CAPABILITY_IAM \
    --region us-east-1
```

## Use the SAM CLI to build and test locally

Build your application with the `sam build` command.

```bash
app$ sam build \
    --template aws-cloudformation/hello-world-serverless-template.yml \
    --manifest ./hello-world-serverless
```

The SAM CLI installs dependencies defined in `hello-world-serverless/package.json`, creates a deployment package, and saves it in the `.aws-cloudformation/build` folder.

Test a single function by invoking it directly with a test event. An event is a JSON document that represents the input that the function receives from the event source. Test events are included in the `events` folder in this project.

Run functions locally and invoke them with the `sam local invoke` command.

```bash
app$ sam local invoke putItemFunction --event events/event.json
```

The SAM CLI can also emulate your application's API. Use the `sam local start-api` to run the API locally on port 3000.

```bash
app$ sam local start-api
app$ curl http://localhost:3000/
```

The SAM CLI reads the application template to determine the API's routes and the functions that they invoke. The `Events` property on each function's definition includes the route and method for each path.

```yaml
      Events:
        HelloWorld:
          Type: Api
          Properties:
            Path: /hello
            Method: get
```

## Add a resource to your application
The application template uses AWS Serverless Application Model (AWS SAM) to define application resources. AWS SAM is an extension of AWS CloudFormation with a simpler syntax for configuring common serverless application resources such as functions, triggers, and APIs. For resources not included in [the SAM specification](https://github.com/awslabs/serverless-application-model/blob/master/versions/2016-10-31.md), you can use standard [AWS CloudFormation](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-template-resource-type-ref.html) resource types.

## Resources

See the [AWS SAM developer guide](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/what-is-sam.html) for an introduction to SAM specification, the SAM CLI, and serverless application concepts.